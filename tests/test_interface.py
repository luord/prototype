import base64
import json
import zlib

import pytest

from server import create_app
from server import interface
from server.matcher import GeneticWordMatcher as Matcher


@pytest.fixture
def client():
    return create_app().test_client()


@pytest.mark.parametrize("data,headers,expected", [
    ({}, {}, b"Word to match:"),
    ({"spa": True}, {}, b"/dist/spa"),
    pytest.param({}, {}, b"/dist/spa", marks=pytest.mark.xfail),
    pytest.param({}, {'HX-Request': True}, b"Luis's", marks=pytest.mark.xfail)
])
def test_root(client, data, headers, expected):
    res = client.get("/", query_string=data, headers=headers)

    assert expected in res.get_data()


def test_step_no_data(client):
    rv = client.get("/step/1", follow_redirects=True)

    assert b"no matcher active" in rv.data


def test_step_invalid_step(client):
    data = {"word": "abc", "mutation": 5, "log": []}
    with client.session_transaction() as sess:
        sess["matcher"] = base64.b64encode(zlib.compress(
            json.dumps(data).encode('utf-8')
        ))

    rv = client.get("/step/2", follow_redirects=True)

    assert b"out of range" in rv.data


def test_step_not_finished(client, monkeypatch):
    data = {"word": "abc", "mutation": 5, "log": [""]*101}
    matcher = Matcher(**data)

    monkeypatch.setattr(interface.Step, "matcher", matcher)
    monkeypatch.setattr(matcher, "crossover", lambda *_: None)

    rv = client.get("/step/2", data=data, headers={"HX-Request": True})

    assert b"Word we're looking for: <em>abc</em>" in rv.data
    assert b"<em>2</em>" in rv.data
    assert b'<a href="/step/1"' in rv.data
    assert b"Next step" in rv.data
    assert b"Luis's" not in rv.data


def test_step_finished(client):
    data = {
        "word": "abc", "mutation": 5, "log": "_"*101, "population": ["abc"]
    }

    rv = client.get("/step/1", data=data)

    assert f"Word <em>{data['word']}</em> found" in str(rv.data)
    assert b"<em>1</em>" in rv.data
    assert b'<a href="/step/2"' in rv.data
    assert b"Next step" not in rv.data


def test_step_invalid_data(client):
    rv = client.get("/step/1", json={"word": "a", "mutation": 5})

    assert rv.status_code == 400
    assert "Word length must be" in rv.get_json()["message"]

    with client.session_transaction() as sess:
        sess["matcher"] = base64.b64encode(zlib.compress(
            json.dumps({"word": "123", "mutation": 1}).encode('utf-8')
        ))

    rv = client.get("/step/1", headers={"HX-Request": True})

    assert b"Word can only have characters in" in rv.data

    client.cookie_jar.clear_session_cookies()
    rv = client.get(
        "/step/1", follow_redirects=True, data={"word": "abc", "mutation": 0}
    )

    assert b"Mutation rate must be between" in rv.data

import pytest

from unittest.mock import MagicMock

from server import matcher as matcher_mod
from server.matcher import (
    GeneticWordMatcher as Matcher,
    Errors,
    Mutation,
    RootWord,
    WinnerPair
)


@pytest.fixture
def random(monkeypatch):
    m = MagicMock()
    monkeypatch.setattr(matcher_mod, "random", m)
    return m


@pytest.fixture
def matcher():
    return Matcher("jon", 10)


@pytest.mark.parametrize("word,mutation,error", [
    ("   ", 10, Errors.WORD_CHARS),
    ("0ab", 5, Errors.WORD_CHARS),
    ("abc", 11, Errors.MUTATION_VALUE),
    ("ab", 0, Errors.WORD_SIZE),
    ("abc", 0, Errors.MUTATION_VALUE),
    ("BAC", 1, Errors.WORD_CHARS),
    ("", 1, Errors.WORD_SIZE),
    ("".join("f" for _ in range(101)), 5, Errors.WORD_SIZE),
    pytest.param("foo", 3, None, marks=pytest.mark.xfail)
])
def test_validation(word: str, mutation: int, error):
    with pytest.raises(ValueError) as err:
        Matcher(RootWord(word), Mutation(mutation))

    assert err.match(str(error))


@pytest.mark.parametrize("population,expected", [
    (["baz", "foo", "bar", "doe"], ("baz", "foo")),
    (["fon", "bar", "foo", "jon"], ("fon", "jon")),
    (["foo", "baz", "doe", "bar"], ("foo", "baz")),
    (["foo", "bar", "baz", "doe"], ("foo", "doe")),
    ([], ("", ""))
])
def test_get_best_matchers(matcher, population, expected):
    matcher.population = population
    assert matcher.get_best_matches() == expected


@pytest.mark.parametrize("seed,int_r,choice_r,found,expected", [
    (["foo", "bar"], [1, 100, 100, 1], [1, "x"], False, {"box"}),
    (["foo", "bar"], [1, 1, 100, 1], [1, "j", "n"], True, {"jon"}),
    (["foo", "bar"], [0, 100, 1, 100], [2, "y"], False, {"fyr"}),
    (["doe", "baz"], [0, 1, 100, 1], [1, "z", "t"], False, {"zat"}),
    (["jon"], [], [], True, set())
])
def test_crossover(
    matcher, random, seed, int_r, choice_r, found, expected
):
    random.randint.side_effect = int_r
    random.choice.side_effect = choice_r

    matcher.crossover(seed)

    assert expected | set(seed) == set(matcher.population)
    assert matcher.found is found


@pytest.mark.parametrize("invalid_cross,mutation", [
    (0, 10),
    (1, 9),
    (9, 1),
    pytest.param(1, 10, marks=pytest.mark.xfail),
    pytest.param(2, 9, marks=pytest.mark.xfail),
    (8, 2),
    (20, 1),
    (900, 1)
])
def test_run(monkeypatch, random, matcher, invalid_cross: int, mutation: int):
    ic = invalid_cross

    def crossover(*_):
        nonlocal ic
        if ic:
            matcher.population = ""
            ic -= 1
            return
        matcher.population = matcher.word

    monkeypatch.setattr(matcher, "crossover", MagicMock(
        side_effect=crossover
    ))
    monkeypatch.setattr(matcher, "get_best_matches", MagicMock(
        return_value=WinnerPair()
    ))
    random.choices.return_value = ""

    while not matcher.found:
        matcher()

    assert matcher.log == [WinnerPair() for _ in range(invalid_cross + 1)]
    assert matcher.mutation == mutation
    assert matcher.found

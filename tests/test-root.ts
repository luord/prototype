import {html} from 'htm/preact'
import {test} from 'tape'
import {cleanup, render, fireEvent} from '@testing-library/preact'

import {Root} from '@spa/root'
import {StoreContext, reducer} from '@spa/store'

test('It works correctly', t => {
  let result = null

  const store = {
    state: {
      word: 'bar',
      mutation: 10
    },
    dispatch: action => {
      result = reducer({}, action)
    }
  }

  const {container, getByRole, getByText, queryByRole} = render(html`
    <${StoreContext.Provider} value=${store}>
      <${Root} />
    <//>
  `)

  t.ok(getByText('Word to match:'))
  t.notOk(queryByRole('listitem'))
  t.notOk(result)

  fireEvent.change(getByRole('textbox'), {target: {value: 'foo'}})

  t.deepEqual(result, {word: 'foo'})

  fireEvent.change(getByRole('spinbutton'), {target: {value: 10}})

  t.deepEqual(result, {mutation: '10'})
  t.end()
  cleanup()
})

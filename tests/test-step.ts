import {html} from 'htm/preact'
import {test} from 'tape'
import {cleanup, fireEvent, render} from '@testing-library/preact'

import {Step} from '@spa/step'
import {StoreContext, reducer, DEFAULT_CONTEXT} from '@spa/store'

test('It draws working correctly', t => {
  const state = {
    word: 'bar',
    mutation: 10,
    log: [['bar']],
    population: ['baz']
  }
  const store = {
    state,
    dispatch: () => null
  }

  const {container, getByText, getByRole} = render(html`
    <${StoreContext.Provider} value=${store}>
      <${Step} step=1 />
    <//>
  `)

  t.ok(getByText('Current mutation rate:'))
  t.ok(getByText('Next step'))
  t.ok(getByRole('link') === getByText('Start over'))
  t.ok(getByRole('button'))
  t.end()
  cleanup()
})

test('It draws complete correctly', t => {
  const state = {
    word: 'bar',
    mutation: 10,
    log: new Array(101),
    population: ['bar']
  }
  const store = {
    state,
    dispatch: () => null
  }

  const {container, getByText, queryByRole} = render(html`
    <${StoreContext.Provider} value=${store}>
      <${Step} step=1 />
    <//>
  `)

  t.ok(getByText('Final mutation rate:'))
  t.throws(() => queryByRole('link'))
  t.notOk(queryByRole('button'))
  t.end()
  cleanup()
})

test('It cleans up on dispatch', t => {
  let reduced = null

  const store = {
    state: {population: [], log: []},
    dispatch: action => {
      reduced = reducer({}, action)
    }
  }

  const {container, getByText} = render(html`
    <${StoreContext.Provider} value=${store}>
      <${Step} step=1 />
    <//>
  `)

  fireEvent.click(getByText('Start over'))

  t.deepEqual(reduced, DEFAULT_CONTEXT)
  t.end()
  cleanup()
})

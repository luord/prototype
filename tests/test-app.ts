import {html} from 'htm/preact'
import {route} from 'preact-router'

import {test} from 'tape'
import fetchMock = require('fetch-mock')
import {cleanup, fireEvent, render} from '@testing-library/preact'

import {App} from '@spa/app'

test('It draws the form', t => {
  const {container, getByText} = render(html`<${App} />`)

  t.ok(getByText('Send'))
  cleanup()
  t.end()
})

test('It moves to step on request success', async t => {
  fetchMock.once('/step/1', {word: 'bar', log: [], population: []}, {overwriteRoutes: true})
  const {container, findByText, getByText, queryByText} = render(html`<${App} />`)

  fireEvent.click(getByText('Send'))

  await findByText('Start over')

  t.notOk(queryByText('Final mutation rate:'))
  cleanup()
  t.end()
})

test('Goes to next step on call from step', async t => {
  fetchMock.once('/step/2', {
    word: 'bar', mutation: 10, log: [], population: ['bar']
  }, {overwriteRoutes: true})

  const {container, findByText, queryByText} = render(html`<${App} />`)

  route('/step/1')

  const submit = await findByText('Next step')

  fireEvent.click(submit)

  await findByText('Final mutation rate:')

  t.notOk(queryByText('Next step'))
  cleanup()
  t.end()
})

test('shows error on request error', async t => {
  const error = 'Internal Server Error'

  fetchMock.once('/step/1', {body: {message: error}, status: 400}, {overwriteRoutes: true})

  const {container, findByText, getByText} = render(html`<${App} />`)

  fireEvent.click(getByText('Send'))

  await findByText(error)

  t.ok(getByText('Word to match:'))
  cleanup()
  t.end()
})


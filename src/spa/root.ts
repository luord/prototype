import {html} from 'htm/preact'
import {useContext, useState} from 'preact/hooks'
import {route} from 'preact-router'

import {StoreContext, match} from '@spa/store'

export function Root() {
  const {state, dispatch} = useContext(StoreContext)
  const [error, setError] = useState('')

  const changeWord = event => {
    dispatch({type: 'update', value: {word: event.target.value}})
  }

  const changeMutation = event => {
    dispatch({type: 'update', value: {mutation: event.target.value}})
  }

  const submitForm = async event => {
    event.preventDefault()
    try {
      const data = await match(state)
      dispatch({type: 'match', value: data})
      route('/step/1', true)
    } catch (error: unknown) {
      setError((error as any).message)
    }
  }

  const errorMessage = html`<li>${error}</li>`

  return html`
    <ul id="errors">${error && errorMessage}</ul>
    <form onSubmit=${submitForm}>
      <label>Word to match:
        <input onChange=${changeWord} value=${state.word} />
      </label>
      <label>Mutation rate:
        <input type="number" onChange=${changeMutation} value=${state.mutation} />
      </label>
      <input type="submit" value="Send" />
    </form>
  `
}

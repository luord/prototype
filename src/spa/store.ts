import {createContext} from 'preact'

export const DEFAULT_CONTEXT = {
  word: '',
  mutation: null,
  log: [],
  population: []
}

export const StoreContext = createContext({
  state: DEFAULT_CONTEXT,
  dispatch: null
})

export async function match(data, page = 1) {
  const response = await fetch(`/step/${page}`, {
    method: 'POST',
    headers: {'content-type': 'application/json'},
    body: JSON.stringify(data)
  })
  const result = await response.json()

  if (!response.ok) {
    throw new Error(result.message)
  }

  return result
}

export const reducer = (state, action) => {
  switch (action.type) {
    case 'match':
      return action.value
    case 'update':
      return {...state, ...action.value}
    case 'clean':
      return DEFAULT_CONTEXT
    default:
      throw new Error('Invalid action')
  }
}

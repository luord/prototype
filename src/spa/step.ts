import {html} from 'htm/preact'
import {useContext} from 'preact/hooks'
import {Link, route} from 'preact-router'

import {StoreContext, match} from '@spa/store'

export function Step({step}) {
  const {state, dispatch} = useContext(StoreContext)

  const page: number = Number.parseInt(step, 10)

  const found = state.population.includes(state.word)

  const submitForm = async event => {
    event.preventDefault()
    const data = await match(state, page + 1)
    dispatch({type: 'match', value: data})
    route(`/step/${page + 1}`, true)
  }

  const cleanState = () => {
    dispatch({type: 'clean'})
  }

  const steps = []
  for (let i = 1; i <= Math.ceil(state.log.length / 100); i++) {
    if (i === page) {
      steps.push(html`<li><em>${i}</li></em>`)
    } else {
      steps.push(html`<li><${Link} href="/step/${i}">${i}<//></li>`)
    }
  }

  const log = state.log.slice((page - 1) * 100, page * 100).map(row => html`
    <tr>${row.map(value => html`<td>${value}</td>`)}</tr>
  `)

  const finished = html`
    <p>Word <em>${state.word}</em> found.</p>
    <p>Final mutation rate: <em>${state.mutation}</em></p>
    <p>Final population: <em>${state.population}</em></p>
  `

  const working = html`
    <p>Word we're looking for: <em>${state.word}</em></p>
    <p>Current mutation rate: <em>${state.mutation}</em></p>
    <p>Current population: <em>${state.population}</em></p>
  `

  const form = html`
    <form onSubmit=${submitForm}>
      <input type="submit" value="Next step" />
    </form>
  `

  return html`
    ${found && finished}
    ${!found && working}
    <h2>Seed log #${step}</h2>
    <table>${log}</table>
    <h2>Steps</h2>
    <ul>${steps}</ul>
    ${!found && form}
    <${Link} onClick=${cleanState} href="/">Start over<//>
  `
}

import {html} from 'htm/preact'
import {useReducer} from 'preact/hooks'
import {Router} from 'preact-router'

import {Root} from '@spa/root'
import {Step} from '@spa/step'
import {DEFAULT_CONTEXT, StoreContext, reducer} from '@spa/store'

export function App() {
  const [state, dispatch] = useReducer(reducer, DEFAULT_CONTEXT)
  const store = {state, dispatch}

  return html`
    <main>
      <${StoreContext.Provider} value=${store}>
        <${Router}>
          <${Step} path="/step/:step+" />
          <${Root} path="/" />
        <//>
      <//>
    </main>
  `
}

import random
from collections import namedtuple
from dataclasses import dataclass, field
from enum import Enum
from string import ascii_lowercase as LOWER


Errors = Enum("Errors", "WORD_CHARS WORD_SIZE MUTATION_VALUE")


class Mutation(int):
    VALID_RANGE = range(1, 10)

    def __new__(cls, value: int):
        if not cls.VALID_RANGE.start <= value <= cls.VALID_RANGE.stop:
            raise ValueError(Errors.MUTATION_VALUE)

        return super(cls, cls).__new__(cls, value)


class RootWord(str):
    VALID_SIZE = range(3, 100)
    VALID_CHARS = set(LOWER)

    def __new__(cls, value: str):
        if not cls.VALID_SIZE.start <= len(value) <= cls.VALID_SIZE.stop:
            raise ValueError(Errors.WORD_SIZE)

        if not set(value).issubset(cls.VALID_CHARS):
            raise ValueError(Errors.WORD_CHARS)

        return super(cls, cls).__new__(cls, value)


WinnerPair = namedtuple("WinnerPair", ("first", "second"), defaults=("", ""))
setattr(WinnerPair, "__bool__", lambda self: any(self))


@dataclass
class GeneticWordMatcher:
    word: RootWord
    mutation: Mutation
    log: list[WinnerPair] = field(default_factory=list)
    population: tuple[str, ...] = field(default_factory=tuple)

    ITERATIONS_PER_CYCLE = 100

    @property
    def found(self) -> bool:
        return self.word in self.population

    def __call__(self) -> None:
        for _ in range(self.ITERATIONS_PER_CYCLE):
            if self.found:
                return None

            seed = self.get_best_matches() or WinnerPair(*(
                "".join(random.choices(LOWER, k=len(self.word)))
                for _ in range(2)
            ))

            # Decrease the mutation rate on each repetition
            self.mutation = Mutation(
                self.mutation - (self.mutation > 1 and self.log[-1:] == [seed])
            )

            self.log.append(seed)

            self.crossover(seed)

    def get_best_matches(self) -> WinnerPair:
        """
        Divide population in half.
        Pick the word closest to the matching word in each half.
        """
        return WinnerPair(*map(
            lambda population_half: max(
                population_half,
                default="",
                key=lambda word: sum(
                    a == b for a, b in zip(word, self.word)
                )
            ),
            (self.population[::2], self.population[1::2])
        ))

    def crossover(self, seed: WinnerPair):
        """
        Randomly combine the two current winners to create a new population.
        Insert random characters not from current winners, using mutation.
        """
        result = set(seed)

        while len(result) < len(self.word) and self.word not in result:
            split = random.choice(range(len(self.word)))
            select = random.randint(0, 1)

            first, second = seed[select], seed[not select]
            child = first[:split] + second[split:]

            child = "".join(
                c if random.randint(1, 100) > self.mutation
                else random.choice(LOWER)
                for c in child
            )

            result.add(child)

        self.population = tuple(result)

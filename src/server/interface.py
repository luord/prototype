import base64
import json
import zlib
from dataclasses import asdict
from functools import cached_property
from math import ceil

from flask import Blueprint, redirect, render_template
from flask.globals import request, session
from flask.helpers import flash, url_for
from flask.views import View

from server.matcher import (
    GeneticWordMatcher as Matcher, Errors, Mutation, RootWord
)


bp = Blueprint("interface", __name__)


class Root(View):
    def dispatch_request(self):
        template = "main" if "HX-Request" in request.headers else "index"

        return render_template(
            f"root/{template}.j2",
            spa=request.args.get("spa")
        )


class Step(View):
    methods = ["GET", "POST"]

    def dispatch_request(self, page):
        try:
            self.matcher()
            pages = ceil(len(self.matcher.log)/Matcher.ITERATIONS_PER_CYCLE)
            assert 1 <= page <= pages, "out of range"
            self.page = page
            self.pages = pages
        except Exception as err:
            return self.error_response(err)

        data = asdict(self.matcher)

        session["matcher"] = base64.b64encode(zlib.compress(
            json.dumps(data).encode()
        ))

        return self.make_response(data)

    def make_response(self, data: dict):
        if request.is_json:
            return data

        if "HX-Request" in request.headers:
            template = render_template(
                "step/main.j2", **{**data, **self.context}
            )
            headers = {
                "HX-Push": url_for("interface.step", page=self.context["page"])
            }
        else:
            template = render_template(
                "step/index.j2", **{**data, **self.context}
            )
            headers = {}

        return template, headers

    def error_response(self, err: Exception):
        if isinstance(err, AssertionError):
            msg = str(err)
        else:
            msg = self._parse_error(err)

        if request.is_json:
            return {"message": msg}, 400

        flash(f"Error: {msg}", category="error")
        if 'HX-Request' in request.headers:
            return render_template("errors.j2", htmx_err=True)
        else:
            return redirect(url_for('interface.root'))

    @cached_property
    def matcher(_) -> Matcher:
        data = request.json or {**request.form} or session.get("matcher", {})
        if isinstance(data, bytes):
            data = json.loads(zlib.decompress(base64.b64decode(data)))

        assert data, "no matcher active"
        word = RootWord(str(data.pop("word")))
        mutation = Mutation(int(data.pop("mutation")))

        return Matcher(word, mutation, **data)

    @cached_property
    def context(self) -> dict:
        return {
            "page": self.page,
            "pages": self.pages,
            "found": self.matcher.found,
            "log": self.matcher.log[slice(*(
                (self.page - v) * Matcher.ITERATIONS_PER_CYCLE for v in (1, 0)
            ))],
            "spa": request.args.get("spa")
        }

    @staticmethod
    def _parse_error(err: Exception):
        if Errors.WORD_SIZE in err.args:
            return (
                f"Word length must be between {RootWord.VALID_SIZE.start}"
                f" and {RootWord.VALID_SIZE.stop} characters."
            )
        if Errors.WORD_CHARS in err.args:
            return (
                f"Word can only have characters in"
                f" {''.join(sorted(RootWord.VALID_CHARS))}"
            )
        if Errors.MUTATION_VALUE in err.args:
            return (
                f"Mutation rate must be between {Mutation.VALID_RANGE.start}"
                f" and {Mutation.VALID_RANGE.stop}"
            )


bp.add_url_rule("/", view_func=Root.as_view("root"))
bp.add_url_rule("/step/<int:page>", view_func=Step.as_view("step"))

# Prototype of a progressively enhanced web application

The application receives two parameters, `word` and `mutation`, and runs a genetic algorithm that finds the word, using `mutation`
to decide how random the characters seeded should initially be.

The back-end is a Flask based Python application. If javascript is enabled, it will use Htmx to enhance the interactions.

Passing the `?spa=true` query string in the root url will load a Preact based single page application.

## Using

A `Makefile` is provided with common tasks. Note the `.venv` target for setting up.

### Running/Building

`make serve` runs the flask application in port `8000`. It watches the `src/server`, `src/templates` and `dist` folders.

`make watch` transpiles and bundles the preact application without compressing. It watches the `src/spa` folder and outputs to `dist`.

`make dist` transpiles and bundles the preact application.

### Testing

`make test` runs the tests for both back-end and front-end.

`make debug` runs the tests with debugging features enabled.

### Linting

`make lint` runs the linters.

# Deployment

The application is deployed automatically on each commit, using GitLab CI, to a Heroku instance. It runs [here](https://luord-prototype.herokuapp.com/).
